//
//  VisualDebug.swift
//  Budgetter
//
//  Created by Liam Dunne on 20/10/2015.
//  Copyright © 2015 Lmd64. All rights reserved.
//

import UIKit

let kBorderWidth = CGFloat(1.0)
let kBorderRadius = CGFloat(4.0)

extension UIView {
    
    func debugViewBorders() {
        self.layer.borderColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.8).CGColor
        self.layer.borderWidth = kBorderWidth
        self.layer.cornerRadius = kBorderRadius
    }

    func debugBorders() {
        self.layer.borderColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.8).CGColor
        self.layer.borderWidth = kBorderWidth
        self.layer.cornerRadius = kBorderRadius
        debugSubviewBounds()
    }
    
    func debugSubviewBounds(){
        let subviews = self.subviews
        let count = subviews.count
        for view in self.subviews {
            let idx = self.subviews.indexOf(view)
            let hue = CGFloat(idx!) / CGFloat(count)
            let _color = UIColor(hue: hue, saturation:1.0, brightness:1.0, alpha:0.8)
            view.layer.borderColor = _color.CGColor
            view.layer.borderWidth = kBorderWidth
            view.layer.cornerRadius = kBorderRadius
            view.debugSubviewBounds()
        }
    }
    
}
