//
//  MasterViewController.swift
//  Budgetter
//
//  Created by Liam Dunne on 19/10/2015.
//  Copyright © 2015 Lmd64. All rights reserved.
//

import UIKit
import CoreData
import DataModel

class MasterViewController: UITableViewController, NSFetchedResultsControllerDelegate, UIAlertViewDelegate {

    var budgetViewController: BudgetViewController? = nil
    var managedObjectContext: NSManagedObjectContext? = nil


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationItem.leftBarButtonItem = self.editButtonItem()

        let addButton = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: "insertNewObject:")
        self.navigationItem.rightBarButtonItem = addButton
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.budgetViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? BudgetViewController
        }
    }

    override func viewWillAppear(animated: Bool) {
        self.clearsSelectionOnViewWillAppear = self.splitViewController!.collapsed
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        print("\(DataModel.logDescription())")
        print("\(DataModel.logSummary())")
        self.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func reloadData(){
        do {
            try self.fetchedResultsController.performFetch()
        } catch {
            print(error)
        }
        self.tableView.reloadData()
    }
    
    // MARK: - Segues

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetail" {
            
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let budget = self.fetchedResultsController.objectAtIndexPath(indexPath) as! Budget
                let controller = (segue.destinationViewController as! UINavigationController).topViewController as! BudgetViewController
                controller.budget = budget
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
                controller.navigationItem.leftItemsSupplementBackButton = true
                
            } else if let budget = sender as? Budget {
                let controller = (segue.destinationViewController as! UINavigationController).topViewController as! BudgetViewController
                controller.budget = budget
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.fetchedResultsController.sections?.count ?? 0
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = self.fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        self.configureCell(cell, atIndexPath: indexPath)
        return cell
    }

    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            let context = self.fetchedResultsController.managedObjectContext
            let budget = self.fetchedResultsController.objectAtIndexPath(indexPath) as! Budget
            context.deleteObject(budget)
                
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                //print("Unresolved error \(error), \(error.userInfo)")
                abort()
            }
            let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows
            tableView.reloadRowsAtIndexPaths(indexPathsForVisibleRows!, withRowAnimation: .Automatic)
        }
    }

    func configureCell(cell: UITableViewCell, atIndexPath indexPath: NSIndexPath) {
        if let budget = self.fetchedResultsController.objectAtIndexPath(indexPath) as? Budget {
            cell.textLabel!.text = budget.name
            cell.textLabel!.font = UIFont.preferredFontForTextStyle(UIFontTextStyleTitle1)
            cell.textLabel!.textColor = UIColor.grayColor()
            
            let (filename,csv) = budget.csvOutput()
            if filename != "" {
                print("\(filename)")
                print("\(csv)")
            }
            
        }
    }

    // MARK: - Fetched results controller

    var fetchedResultsController: NSFetchedResultsController {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        _fetchedResultsController = DataModel.sharedInstance.fetchedResultsController
                
        return _fetchedResultsController!
    }    
    var _fetchedResultsController: NSFetchedResultsController? = nil

    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        self.tableView.beginUpdates()
    }

    func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        switch type {
            case .Insert:
                self.tableView.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
            case .Delete:
                self.tableView.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
            default:
                return
        }
    }

    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        switch type {
            case .Insert:
                tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Fade)
            case .Delete:
                tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
            case .Update:
                self.configureCell(tableView.cellForRowAtIndexPath(indexPath!)!, atIndexPath: indexPath!)
            case .Move:
                tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
                tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Fade)
        }
    }

    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        self.tableView.endUpdates()
    }

    /*
     // Implementing the above methods to update the table view in response to individual changes may have performance implications if a large number of changes are made simultaneously. If this proves to be an issue, you can instead just implement controllerDidChangeContent: which notifies the delegate that all section and object changes have been processed.
     
     func controllerDidChangeContent(controller: NSFetchedResultsController) {
         // In the simplest, most efficient, case, reload the table view.
         self.tableView.reloadData()
     }
     */

    func insertNewObject(sender: AnyObject) {
        let alert = UIAlertController(title: "Create", message: "Create a new user or a new budget", preferredStyle: .ActionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: { (_) in
        })

        let budgetAction = UIAlertAction(title: "Budget", style: .Default) { (_) in
            self.insertNewBudget(sender)
        }
        let userAction = UIAlertAction(title: "User", style: .Default) { (_) in
            self.insertNewUser(sender)
        }
        
        alert.addAction(budgetAction)
        alert.addAction(userAction)
        alert.addAction(cancelAction)

        self.presentViewController(alert, animated: true){
        }

    }

    func insertNewUser(sender: AnyObject) {
        let alert = UIAlertController(title: "New User", message: "Enter a new username", preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: { (_) in
        })
        
        
        let addAction = UIAlertAction(title: "Add", style: .Default) { (_) in
            let nameTextField = alert.textFields![0] as UITextField
            let name = nameTextField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            User.new(name!)
            
            self.reloadData()
            
        }
        addAction.enabled = false
        
        alert.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = "Enter Name"
            textField.font = UIFont.preferredFontForTextStyle(UIFontTextStyleTitle3)
            textField.autocapitalizationType = .Words
            textField.autocorrectionType = .Yes
            
            NSNotificationCenter.defaultCenter().addObserverForName(UITextFieldTextDidChangeNotification, object: textField, queue: NSOperationQueue.mainQueue()) { (notification) in
                addAction.enabled = textField.text != ""
            }
            textField.becomeFirstResponder()
        }
        
        alert.addAction(addAction)
        alert.addAction(cancelAction)
        
        self.presentViewController(alert, animated: true){
        }
        
        
    }
    
    func insertNewBudget(sender: AnyObject) {
        let alert = UIAlertController(title: "New Budget", message: "Enter a name for your new budget", preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: { (_) in
        })
        
        
        let addAction = UIAlertAction(title: "Add", style: .Default) { (_) in
            let nameTextField = alert.textFields![0] as UITextField
            let name = nameTextField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            Budget.new(name!)
            
            self.reloadData()
            
//            let indexPath = self.fetchedResultsController.indexPathForObject(budget)
//            self.performSegueWithIdentifier("showDetail", sender: indexPath)
            
        }
        addAction.enabled = false
        
        alert.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = "Budget Name"
            textField.font = UIFont.preferredFontForTextStyle(UIFontTextStyleTitle3)
            textField.autocapitalizationType = .Words
            textField.autocorrectionType = .Yes
            
            NSNotificationCenter.defaultCenter().addObserverForName(UITextFieldTextDidChangeNotification, object: textField, queue: NSOperationQueue.mainQueue()) { (notification) in
                addAction.enabled = textField.text != ""
            }
            textField.becomeFirstResponder()
        }
        
        alert.addAction(addAction)
        alert.addAction(cancelAction)
        
        self.presentViewController(alert, animated: true){
        }
        
    }
    
}

