//
//  CostItemsViewController.swift
//  Budgetter
//
//  Created by Liam Dunne on 20/10/2015.
//  Copyright © 2015 Lmd64. All rights reserved.
//

import UIKit
import CoreData
import DataModel

public class ItemHeaderView: UICollectionReusableView {
    @IBOutlet weak var shadedView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
}

public class ItemCell: UICollectionViewCell {
    @IBOutlet weak var shadedView: UIView!
    @IBOutlet public weak var titleLabel: UILabel!
    @IBOutlet public weak var amountLabel: UILabel!
}

public class AddItemCell: UICollectionViewCell {
    @IBOutlet weak var shadedView: UIView!
}

public class ItemFooterView: UICollectionReusableView {
    @IBOutlet weak var shadedView: UIView!
    @IBOutlet weak var totalTitleLabel: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var percentTitleLabel: UILabel!
    @IBOutlet weak var percentAmountLabel: UILabel!
}

public class CostItemsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: KDDragAndDropCollectionView!
    //@IBOutlet weak var flowLayout: UICollectionViewFlowLayout!
    //@IBOutlet weak var flowLayout: CostItemsReorderableFlowLayout!
    @IBOutlet weak var flowLayout: UICollectionViewFlowLayout!
    @IBOutlet public weak var collectionViewWidth: NSLayoutConstraint!
    
    @IBOutlet weak var topFadeView: FadeView!
    @IBOutlet weak var bottomFadeView: FadeView!

    let headerBackgroundColor = UIColor(white: 0.9, alpha: 1.0)
    let costItemBackgroundColor1 = UIColor(white: 0.99, alpha: 1.0)
    let costItemBackgroundColor2 = UIColor(white: 0.97, alpha: 1.0)
    let rowHeight = CGFloat(50.0)
    let headerHeight = CGFloat(32.0)
    let footerHeight = CGFloat(96.0)

    var budget: Budget? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }
    
    var user: User? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }

    func configureView() {
        // Update the user interface for the detail item.
        
        if let fadeView = topFadeView {
            fadeView.isTop = true
            fadeView.setNeedsDisplay()
            fadeView.debugBorders()
        }
        if let fadeView = bottomFadeView {
            fadeView.isTop = false
            fadeView.setNeedsDisplay()
            fadeView.debugBorders()
        }
        
        if budget != nil && user != nil {
            do {
                try costItemsFetchedResultsController.performFetch()
            } catch {
                print(error)
            }
            
            if let flowLayout = self.flowLayout {
                let columnWidth = CGRectGetWidth(collectionView.frame)
                flowLayout.itemSize = CGSizeMake(columnWidth-0.1, rowHeight)
                flowLayout.headerReferenceSize = CGSizeMake(columnWidth-0.1, headerHeight)
                flowLayout.footerReferenceSize = CGSizeMake(columnWidth-0.1, footerHeight)
                flowLayout.sectionInset = UIEdgeInsetsZero
                flowLayout.sectionHeadersPinToVisibleBounds = true
                flowLayout.sectionFootersPinToVisibleBounds = true
                flowLayout.invalidateLayout()
            }
            
            setAlphaForContentOffset()

            view.setNeedsDisplay()
            view.setNeedsLayout()
            reloadData()
        }
    }
    
    func reloadData(){
        collectionView.reloadData()
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }

    public override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //add costItem
    @IBAction func addCostItem(sender: AnyObject){
        
        let alert = UIAlertController(title: "Add Cost", message: "Add cost", preferredStyle: .Alert)
        
        let addAction = UIAlertAction(title: "Add", style: .Default) { (_) in
            let nameTextField = alert.textFields![0] as UITextField
            let amountTextField = alert.textFields![1] as UITextField
            
            nameTextField.resignFirstResponder()
            amountTextField.resignFirstResponder()
            
            let name = nameTextField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            
            let amountString = amountTextField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            let amount = Double(amountString!)
            
            let user = self.budget?.users?.allObjects.first as? User
            
            CostItem.new(name!, amount: amount!, user: user!)
            self.configureView()
        }
        
        addAction.enabled = false
        
        alert.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = "Enter Name"
            textField.font = UIFont.preferredFontForTextStyle(UIFontTextStyleTitle3)
            textField.autocapitalizationType = .Words
            textField.keyboardType = .Default
            textField.autocorrectionType = .Yes
            
            NSNotificationCenter.defaultCenter().addObserverForName(UITextFieldTextDidChangeNotification, object: textField, queue: NSOperationQueue.mainQueue()) { (notification) in
                let nameTextField = alert.textFields![0] as UITextField
                let amountTextField = alert.textFields![1] as UITextField
                addAction.enabled = nameTextField.text != "" && amountTextField.text != ""
            }
            textField.becomeFirstResponder()
        }
        
        alert.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = "Enter Amount"
            textField.font = UIFont.preferredFontForTextStyle(UIFontTextStyleTitle3)
            textField.keyboardType = .DecimalPad
            
            NSNotificationCenter.defaultCenter().addObserverForName(UITextFieldTextDidChangeNotification, object: textField, queue: NSOperationQueue.mainQueue()) { (notification) in
                let nameTextField = alert.textFields![0] as UITextField
                let amountTextField = alert.textFields![1] as UITextField
                addAction.enabled = nameTextField.text != "" && amountTextField.text != ""
            }
        }
        
        alert.addAction(addAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: { (_) in })
        alert.addAction(cancelAction)
        
        self.presentViewController(alert, animated: true){}
        
    }

    func editCostItem(costItem :CostItem){

        let name = costItem.name! as String
        
        let alert = UIAlertController(title: "\(name)", message: "Edit cost", preferredStyle: .Alert)
        
        let doneAction = UIAlertAction(title: "Done", style: .Default) { (_) in
            let nameTextField = alert.textFields![0] as UITextField
            let amountTextField = alert.textFields![1] as UITextField
            
            if nameTextField.isFirstResponder() {
                nameTextField.resignFirstResponder()
            }
            if amountTextField.isFirstResponder() {
                amountTextField.resignFirstResponder()
            }
            
            let name = nameTextField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            let amountString = amountTextField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            let amount = Double(amountString!)
            
            costItem.name = name
            costItem.amount = amount
            DataModel.sharedInstance.saveContext()
            
            self.configureView()
        }
        doneAction.enabled = false
        alert.addTextFieldWithConfigurationHandler { (textField) in
            textField.text = costItem.name
            textField.font = UIFont.preferredFontForTextStyle(UIFontTextStyleTitle3)
            textField.placeholder = "Enter Name"
            textField.autocapitalizationType = .Words
            textField.keyboardType = .Default
            textField.autocorrectionType = .Yes
            
            NSNotificationCenter.defaultCenter().addObserverForName(UITextFieldTextDidChangeNotification, object: textField, queue: NSOperationQueue.mainQueue()) { (notification) in
                doneAction.enabled = textField.text != ""
            }
            textField.becomeFirstResponder()
        }
        alert.addTextFieldWithConfigurationHandler { (textField) in
            textField.text = costItem.amount?.stringValue
            textField.font = UIFont.preferredFontForTextStyle(UIFontTextStyleTitle3)
            textField.placeholder = "Enter Amount"
            textField.keyboardType = .DecimalPad
            
            NSNotificationCenter.defaultCenter().addObserverForName(UITextFieldTextDidChangeNotification, object: textField, queue: NSOperationQueue.mainQueue()) { (notification) in
                doneAction.enabled = textField.text != ""
            }
        }
        alert.addAction(doneAction)
        
        let deleteAction = UIAlertAction(title: "Delete", style: .Destructive, handler: { (_) in
            let nameTextField = alert.textFields![0] as UITextField
            let amountTextField = alert.textFields![1] as UITextField
            
            if nameTextField.isFirstResponder() {
                nameTextField.resignFirstResponder()
            }
            if amountTextField.isFirstResponder() {
                amountTextField.resignFirstResponder()
            }
            self.confirmDeleteCostItem(costItem)
        })
        alert.addAction(deleteAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: { (_) in
        })
        alert.addAction(cancelAction)
        
        self.presentViewController(alert, animated: true){}
        
    }

    func confirmDeleteCostItem(costItem :CostItem) {
        let name = costItem.name! as String
        let alert = UIAlertController(title: "Delete \(name)", message: "Are you sure?", preferredStyle: .Alert)
        let okAction = UIAlertAction(title: "Delete", style: .Destructive, handler: { (_) in
            let context = self.costItemsFetchedResultsController.managedObjectContext
            context.deleteObject(costItem)
            self.configureView()
        })
        alert.addAction(okAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: { (_) in })
        alert.addAction(cancelAction)
        
        self.presentViewController(alert, animated: true){}
    }
    
    //core data
    var costItemsFetchedResultsController: NSFetchedResultsController {
        if _costItemsFetchedResultsController != nil {
            return _costItemsFetchedResultsController!
        }
        _costItemsFetchedResultsController = DataModel.sharedInstance.costItemsFetchedResultsController(budget!, user: user!)
        print("_costItemsFetchedResultsController = \(_costItemsFetchedResultsController)")
        print("user?.name = \(user?.name)")
        return _costItemsFetchedResultsController!
    }
    var _costItemsFetchedResultsController: NSFetchedResultsController? = nil

    func hasCostItems() -> Bool {
        return costItemsFetchedResultsController.sections!.count > 0
    }
    
    // collection view datasource methods
    public func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        if budget != nil {
            if hasCostItems() {
                let numberOfSectionsInCollectionView = costItemsFetchedResultsController.sections!.count
                return numberOfSectionsInCollectionView
            } else {
                return 1
            }
        } else {
            return 0
        }
    }
    
    public func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if hasCostItems() {
            let numberOfItemsInSection = costItemsFetchedResultsController.sections![section].numberOfObjects + 1
            return numberOfItemsInSection
        } else {
            return 1
        }
    }
    
    
    //cell
    public func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        if hasCostItems() {
            let section = indexPath.section
            let row = indexPath.row
            let numberOfItemsInSection = costItemsFetchedResultsController.sections![section].numberOfObjects + 1
            if row != numberOfItemsInSection-1 {
                let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ItemCell", forIndexPath: indexPath)
                configureItemCell(cell, atIndexPath: indexPath)
                return cell
            } else {
                let cell = collectionView.dequeueReusableCellWithReuseIdentifier("AddItemCell", forIndexPath: indexPath)
                configureAddItemCell(cell, atIndexPath: indexPath)
                return cell
            }
        } else {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("AddItemCell", forIndexPath: indexPath)
            configureAddItemCell(cell, atIndexPath: indexPath)
            return cell
        }
    }

    func configureItemCell(cell: UICollectionViewCell, atIndexPath indexPath: NSIndexPath) {
        if let itemCell :ItemCell = cell as? ItemCell {

            itemCell.shadedView.backgroundColor = backgroundColorForItemAtIndexPath(indexPath)
            if let costItem = costItemsFetchedResultsController.objectAtIndexPath(indexPath) as? CostItem {
                itemCell.titleLabel.text = costItem.name
                itemCell.amountLabel.text = costItem.amountFormattedString()
            }
        }
    }
    
    func configureAddItemCell(cell: UICollectionViewCell, atIndexPath indexPath: NSIndexPath) {
        if let itemCell :AddItemCell = cell as? AddItemCell {
            itemCell.shadedView.backgroundColor = backgroundColorForItemAtIndexPath(indexPath)
        }
    }
    
    func backgroundColorForItemAtIndexPath(indexPath :NSIndexPath) -> UIColor {
        if indexPath.row % 2 == 0 {
            return costItemBackgroundColor1
        } else {
            return costItemBackgroundColor2
        }
    }
    
    public func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let section = indexPath.section
        let row = indexPath.row
        let numberOfItemsInSection = costItemsFetchedResultsController.sections![section].numberOfObjects + 1
        if row != numberOfItemsInSection-1 {
            
//            let cell = collectionView.cellForItemAtIndexPath(indexPath)
//            if let itemCell :ItemCell = cell as? ItemCell {
//                itemCell.shadedView.alpha = 0.0
//                UIView.animateWithDuration(0.5, animations: {
//                    itemCell.shadedView.alpha = 1.0
//                })
//            }
            
            if let costItem = costItemsFetchedResultsController.objectAtIndexPath(indexPath) as? CostItem {
                editCostItem(costItem)
            }
            collectionView.deselectItemAtIndexPath(indexPath, animated: true)
            
        } else {
            collectionView.deselectItemAtIndexPath(indexPath, animated: false)
        }
    }
    
    //cell layout
    public func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize{
        let width = CGRectGetWidth(self.view.frame) - 0.1
        let height = CGFloat(rowHeight)
        let size = CGSizeMake(width,height)
        return size
    }

    public func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsZero
    }
    
    public func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return CGFloat(0.0)
    }
    
    public func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return CGFloat(0.0)
    }
    
    
    //supplementary view
    public func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let width = CGRectGetWidth(self.view.frame) - 0.1
        let height = CGFloat(rowHeight * 1.2)
        let size = CGSizeMake(width,height)
        return size
    }
    
    public func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        if kind == UICollectionElementKindSectionHeader {
            let view = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionHeader, withReuseIdentifier: "ItemHeaderView", forIndexPath: indexPath)
            configureHeader(view, atIndexPath: indexPath)
            return view
        } else {
            let view = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionFooter, withReuseIdentifier: "ItemFooterView", forIndexPath: indexPath)
            configureFooter(view, atIndexPath: indexPath)
            return view
        }
        
    }
    
    func configureHeader(view: UICollectionReusableView, atIndexPath indexPath: NSIndexPath) {
        if let view :ItemHeaderView = view as? ItemHeaderView {
            view.shadedView.backgroundColor = headerBackgroundColor
            if let name :String = self.user?.name {
                view.titleLabel.text = name
            }
        }
    }
    
    func configureFooter(view: UICollectionReusableView, atIndexPath indexPath: NSIndexPath) {
        if let view :ItemFooterView = view as? ItemFooterView {
            view.shadedView.backgroundColor = headerBackgroundColor
            if let user :User = self.user {
                if let name :String = self.user?.name {
                    view.totalAmountLabel.text = "\(name)'s total:"
                    view.percentTitleLabel.text = "\(name)'s %:"
                } else {
                    view.totalTitleLabel.text = "Total:"
                    view.percentTitleLabel.text = "%:"
                }
                view.totalAmountLabel.text = budget?.totalFormattedStringForUser(user)
                view.percentAmountLabel.text = budget?.percentFormattedStringForUser(user)
            }
        }
    }

    //scrollview delegate methods
    public func scrollViewDidScroll(scrollView: UIScrollView) {
        setAlphaForContentOffset()
    }
    
    func setAlphaForContentOffset(){
        if let collectionView = self.collectionView {
            
            let viewH = view.frame.size.height

            let contentOffsetY = collectionView.contentOffset.y
            let contentSizeH = collectionView.contentSize.height
            
            if let fadeView = topFadeView {
                let fadeH = CGRectGetHeight(fadeView.frame)
                let scaledOffset = contentOffsetY/fadeH
                //let alpha = CGFloat(min(max(scaledOffset, 0.0), 1.0))
                let alpha = CGFloat(scaledOffset)
                fadeView.alpha = alpha
            }
            
            if let fadeView = bottomFadeView {
                let fadeH = CGRectGetHeight(fadeView.frame)
                let scaledOffset = (viewH - contentSizeH - contentOffsetY + fadeH)/fadeH
                let alpha = CGFloat(scaledOffset)
                fadeView.alpha = alpha
            }
        }
    }

    //cell reordering
    func collectionView(collectionView: UICollectionView, allowMoveAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func collectionView(collectionView: UICollectionView, atIndexPath: NSIndexPath, didMoveToIndexPath toIndexPath: NSIndexPath) {

        let atCostItem = costItemsFetchedResultsController.objectAtIndexPath(atIndexPath) as? CostItem
        let toCostItem = costItemsFetchedResultsController.objectAtIndexPath(toIndexPath) as? CostItem

        let atRow = atIndexPath.row
        let toRow = toIndexPath.row
        
        atCostItem?.index = toRow
        toCostItem?.index = atRow
        
        collectionView.reloadItemsAtIndexPaths([atIndexPath,toIndexPath])
        
//        let costItem = video.clips.removeAtIndex(atIndexPath.row)
//        video.clips.insert(clip, atIndex: toIndexPath.row)
    }

    
}
