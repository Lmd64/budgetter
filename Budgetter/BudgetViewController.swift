//
//  BudgetViewController.swift
//  Budgetter
//
//  Created by Liam Dunne on 19/10/2015.
//  Copyright © 2015 Lmd64. All rights reserved.
//

import UIKit
import CoreData
import DataModel

class BudgetViewController: UIViewController {

    @IBOutlet weak var addButton: UIBarButtonItem!

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var stackView: UIStackView!

    @IBOutlet weak var totalView: UIView!
    @IBOutlet weak var totalCaptionLabel: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
    
    var costItemsControllers :Array<CostItemsViewController> = []
    var dragAndDropManager : KDDragAndDropManager?

    var budget: Budget? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.contentSize = stackView.frame.size
    }
    
    func configureView() {
        // Update the user interface for the detail item.
        if budget != nil {
            do {
                try usersFetchedResultsController.performFetch()
            } catch {
                print(error)
            }
            
            self.title = budget?.name
            totalAmountLabel?.text = budget?.totalFormattedString()
            
            if stackView != nil {
                resetCostItemControllers()
                
                let users = usersFetchedResultsController.fetchedObjects as! [User]
                print(users)
                for user in users {
                    addCostItemControllerForUser(user)
                }
    
                let columnWidth = self.columnWidth()
                for costItemsViewController in costItemsControllers {
                    costItemsViewController.collectionViewWidth.constant = columnWidth
                }

                reloadData()

                view.setNeedsDisplay()
                view.setNeedsLayout()

            }

        } else {
            totalAmountLabel?.text = ""
            resetCostItemControllers()
        }

    }

    func resetCostItemControllers(){
        for costItemsViewController in costItemsControllers {
            costItemsViewController.view.removeFromSuperview()
        }
        costItemsControllers = []
    }

    func addCostItemControllerForUser(user :User){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewControllerWithIdentifier("CostItemsViewController") as! CostItemsViewController
        controller.budget = self.budget
        controller.user = user
        
        addChildViewController(controller)
        costItemsControllers.append(controller)

        controller.loadView()
        let view = controller.view
        let index = costItemsControllers.indexOf(controller) as Int?
        stackView.insertArrangedSubview(view, atIndex: index!)
        
        let columnWidth = self.columnWidth()
        controller.collectionViewWidth.constant = columnWidth
        let heightConstraint = NSLayoutConstraint(item: view, attribute: .Height, relatedBy: .Equal, toItem: stackView, attribute: .Height, multiplier: 1.0, constant: 0.0)
        stackView.addConstraint(heightConstraint)
    }

    
    func reloadData(){
        for costItemsController in costItemsControllers {
            print("\(costItemsController.user?.name) has \(costItemsController.user?.costItem?.count)")
            costItemsController.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        //collectionView.registerClass(ItemCell.self, forCellWithReuseIdentifier: "ItemCell")
        
        configureView()
        //self.dragAndDropManager = KDDragAndDropManager(canvas: self.view, collectionViews: costItemsControllers)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func columnWidth() -> CGFloat {
        //let columnWidth = CGRectGetWidth(view.frame)*2.0/3.0
        let columnWidth = CGRectGetWidth(view.frame)/2.0
        return columnWidth
    }
    
    @IBAction func didTapAddButton(sender: AnyObject) {
        if User.allObjects().count == 0 {
            //add user if none
            self.addNewUser(sender)
            
        } else if budget?.users?.count<3 {
            chooseNewUserOrNewCostItem(sender)
            
        } else {
            addCostItem(sender)
        }
    }

    func chooseNewUserOrNewCostItem(sender: AnyObject){
        let alert = UIAlertController(title: "Add User/Item", message: "Add another user or a cost item", preferredStyle: .ActionSheet)
        
        let addUserAction = UIAlertAction(title: "User", style: .Default, handler: { (_) in
            self.addUser(sender)
        })
        alert.addAction(addUserAction)
        
        let addCostAction = UIAlertAction(title: "Cost", style: .Default, handler: { (_) in
            self.addCostItem(sender)
        })
        alert.addAction(addCostAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: { (_) in })
        alert.addAction(cancelAction)
        
        self.presentViewController(alert, animated: true){
        }

    }
    
    //add user
    func addUser(sender: AnyObject){

        var message = "Add new user"
        if User.allObjects().count != 0 {
            message = "Add new or existing user"
        }
        let alert = UIAlertController(title: "Add User", message: message, preferredStyle: .ActionSheet)
        
        let newAction = UIAlertAction(title: "New", style: .Default) { (_) in
            self.addNewUser(sender)
        }
        alert.addAction(newAction)

        if User.allObjects().count != 0 {
            let existingAction = UIAlertAction(title: "Existing", style: .Default) { (_) in
                self.addExistingUser(sender)
            }
            alert.addAction(existingAction)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: { (_) in })
        alert.addAction(cancelAction)
        
        self.presentViewController(alert, animated: true){
        }
        
    }

    func addNewUser(sender: AnyObject) {
        let alert = UIAlertController(title: "New User", message: "Enter a new username", preferredStyle: .Alert)
        
        let addAction = UIAlertAction(title: "Add", style: .Default) { (_) in
            let nameTextField = alert.textFields![0] as UITextField

            nameTextField.resignFirstResponder()

            let name = nameTextField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            let user = User.new(name!)
            self.addUserToBudget(user)
        }
        addAction.enabled = false
        
        alert.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = "Enter Name"
            textField.font = UIFont.preferredFontForTextStyle(UIFontTextStyleTitle3)
            textField.autocapitalizationType = .Words
            textField.autocorrectionType = .Yes
            
            NSNotificationCenter.defaultCenter().addObserverForName(UITextFieldTextDidChangeNotification, object: textField, queue: NSOperationQueue.mainQueue()) { (notification) in
                addAction.enabled = textField.text != ""
            }
            textField.becomeFirstResponder()
        }
        
        alert.addAction(addAction)

        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: { (_) in })
        alert.addAction(cancelAction)
        
        self.presentViewController(alert, animated: true){
        }
        
        
    }

    func addExistingUser(sender: AnyObject){
        
        let alert = UIAlertController(title: "Add User", message: "Existing users", preferredStyle: .ActionSheet)
        
        let existingUsers = User.allObjects()
        for user in existingUsers {
            let isAlreadyAssignedToBudget = budget?.users?.containsObject(user)
            if isAlreadyAssignedToBudget==false {
                let newAction = UIAlertAction(title: user.name, style: .Default) { (_) in
                    self.addUserToBudget(user)
                }
                alert.addAction(newAction)
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: { (_) in })
        alert.addAction(cancelAction)
        
        self.presentViewController(alert, animated: true){
        }
        
    }

    func addUserToBudget(user :User){
        budget?.addUser(user)
        DataModel.sharedInstance.saveContext()
        self.configureView()
    }
    
    //add costItem
    @IBAction func addCostItem(sender: AnyObject){
        
        let alert = UIAlertController(title: "Add Cost", message: "Add cost", preferredStyle: .Alert)
        
        let addAction = UIAlertAction(title: "Add", style: .Default) { (_) in
            let nameTextField = alert.textFields![0] as UITextField
            let amountTextField = alert.textFields![1] as UITextField

            nameTextField.resignFirstResponder()
            amountTextField.resignFirstResponder()
            
            let name = nameTextField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())

            let amountString = amountTextField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            let amount = Double(amountString!)
            
            let user = self.budget?.users?.allObjects.first as? User
            
            CostItem.new(name!, amount: amount!, user: user!)
            self.configureView()
        }
        
        addAction.enabled = false
        
        alert.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = "Enter Name"
            textField.font = UIFont.preferredFontForTextStyle(UIFontTextStyleTitle3)
            textField.autocapitalizationType = .Words
            textField.keyboardType = .Default
            textField.autocorrectionType = .Yes
            
            NSNotificationCenter.defaultCenter().addObserverForName(UITextFieldTextDidChangeNotification, object: textField, queue: NSOperationQueue.mainQueue()) { (notification) in
                let nameTextField = alert.textFields![0] as UITextField
                let amountTextField = alert.textFields![1] as UITextField
                addAction.enabled = nameTextField.text != "" && amountTextField.text != ""
            }
            textField.becomeFirstResponder()
        }
        
        alert.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = "Enter Amount"
            textField.font = UIFont.preferredFontForTextStyle(UIFontTextStyleTitle3)
            textField.keyboardType = .DecimalPad
            
            NSNotificationCenter.defaultCenter().addObserverForName(UITextFieldTextDidChangeNotification, object: textField, queue: NSOperationQueue.mainQueue()) { (notification) in
                let nameTextField = alert.textFields![0] as UITextField
                let amountTextField = alert.textFields![1] as UITextField
                addAction.enabled = nameTextField.text != "" && amountTextField.text != ""
            }
        }
        
        alert.addAction(addAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: { (_) in })
        alert.addAction(cancelAction)
        
        self.presentViewController(alert, animated: true){
        }
        
    }
    
    func editCostItem(costItem :CostItem){

        let name = costItem.name! as String
        
        let alert = UIAlertController(title: "\(name)", message: "Edit cost", preferredStyle: .Alert)
        
        let doneAction = UIAlertAction(title: "Done", style: .Default) { (_) in
            let nameTextField = alert.textFields![0] as UITextField
            let amountTextField = alert.textFields![1] as UITextField

            nameTextField.resignFirstResponder()
            amountTextField.resignFirstResponder()

            let name = nameTextField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())

            let amountString = amountTextField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            let amount = Double(amountString!)
            
            costItem.name = name
            costItem.amount = amount
            DataModel.sharedInstance.saveContext()
            
            self.configureView()
        }
        doneAction.enabled = false
        alert.addTextFieldWithConfigurationHandler { (textField) in
            textField.text = costItem.name
            textField.font = UIFont.preferredFontForTextStyle(UIFontTextStyleTitle3)
            textField.placeholder = "Enter Name"
            textField.autocapitalizationType = .Words
            textField.keyboardType = .Default
            textField.autocorrectionType = .Yes
            
            NSNotificationCenter.defaultCenter().addObserverForName(UITextFieldTextDidChangeNotification, object: textField, queue: NSOperationQueue.mainQueue()) { (notification) in
                doneAction.enabled = textField.text != ""
            }
            textField.becomeFirstResponder()
        }
        alert.addTextFieldWithConfigurationHandler { (textField) in
            textField.text = costItem.amount?.stringValue
            textField.font = UIFont.preferredFontForTextStyle(UIFontTextStyleTitle3)
            textField.placeholder = "Enter Amount"
            textField.keyboardType = .DecimalPad
            
            NSNotificationCenter.defaultCenter().addObserverForName(UITextFieldTextDidChangeNotification, object: textField, queue: NSOperationQueue.mainQueue()) { (notification) in
                doneAction.enabled = textField.text != ""
            }
        }
        alert.addAction(doneAction)
        
        let deleteAction = UIAlertAction(title: "Delete", style: .Destructive, handler: { (_) in
            self.confirmDeleteCostItem(costItem)
        })
        alert.addAction(deleteAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: { (_) in })
        alert.addAction(cancelAction)
        
        self.presentViewController(alert, animated: true){
        }
        
    }
    
    func confirmDeleteCostItem(costItem :CostItem) {
        let name = costItem.name! as String
        let alert = UIAlertController(title: "Delete \(name)", message: "Are you sure?", preferredStyle: .Alert)
        let okAction = UIAlertAction(title: "Delete", style: .Destructive, handler: { (_) in
            let context = self.usersFetchedResultsController.managedObjectContext
            context.deleteObject(costItem)
            self.configureView()
        })
        alert.addAction(okAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: { (_) in })
        alert.addAction(cancelAction)
        
        self.presentViewController(alert, animated: true){}
    }

    //core data
    var usersFetchedResultsController: NSFetchedResultsController {
        if _usersFetchedResultsController != nil {
            return _usersFetchedResultsController!
        }
        _usersFetchedResultsController = DataModel.sharedInstance.usersFetchedResultsController(budget!)
        return _usersFetchedResultsController!
    }
    var _usersFetchedResultsController: NSFetchedResultsController? = nil

    
}

