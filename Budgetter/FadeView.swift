//
//  FadeView.swift
//  Budgetter
//
//  Created by Liam Dunne on 22/10/2015.
//  Copyright © 2015 Lmd64. All rights reserved.
//

import UIKit

@IBDesignable

public class FadeView :UIView {

    @IBInspectable public var isTop :Bool = true {
        didSet {
            //setNeedsDisplay()
        }
    }
    
    @IBInspectable var fadeColor: UIColor = UIColor.redColor() {
        didSet {
            //layer.borderColor = fadeColor?.CGColor
        }
    }
    
    public override func prepareForInterfaceBuilder(){
        
    }
    
    public override func drawRect(rect: CGRect) {
        if isTop {
            drawTopFade(frame: rect)
        } else {
            drawBottomFade(frame: rect)
        }
    }
    
    func drawBottomFade(frame frame: CGRect = CGRectMake(0, 0, 240, 120)) {
        //// General Declarations
        let context = UIGraphicsGetCurrentContext()
        
        //// Color Declarations
        let baseColor = fadeColor
        let baseColorWithOpacity = fadeColor.colorWithAlphaComponent(0)
        
        //// Gradient Declarations
        let fadeOutGradient = CGGradientCreateWithColors(CGColorSpaceCreateDeviceRGB(), [baseColor.CGColor, baseColorWithOpacity.CGColor], [0, 1])!
        
        //// Rectangle Drawing
        let rectangleRect = CGRectMake(frame.minX, frame.minY, frame.width, frame.height)
        let rectanglePath = UIBezierPath(rect: rectangleRect)
        CGContextSaveGState(context)
        rectanglePath.addClip()
        CGContextDrawLinearGradient(context, fadeOutGradient,
            CGPointMake(rectangleRect.midX, rectangleRect.maxY),
            CGPointMake(rectangleRect.midX, rectangleRect.minY),
            CGGradientDrawingOptions())
        CGContextRestoreGState(context)
    }

    func drawTopFade(frame frame: CGRect = CGRectMake(0, 0, 240, 120)) {
        //// General Declarations
        let context = UIGraphicsGetCurrentContext()
        
        //// Color Declarations
        let baseColor = fadeColor
        let baseColorWithOpacity = fadeColor.colorWithAlphaComponent(0)
        
        //// Gradient Declarations
        let fadeOutGradient = CGGradientCreateWithColors(CGColorSpaceCreateDeviceRGB(), [baseColor.CGColor, baseColorWithOpacity.CGColor], [0, 1])!
        
        //// Rectangle Drawing
        let rectangleRect = CGRectMake(frame.minX, frame.minY, frame.width, frame.height)
        let rectanglePath = UIBezierPath(rect: rectangleRect)
        CGContextSaveGState(context)
        rectanglePath.addClip()
        CGContextDrawLinearGradient(context, fadeOutGradient,
            CGPointMake(rectangleRect.midX, rectangleRect.minY),
            CGPointMake(rectangleRect.midX, rectangleRect.maxY),
            CGGradientDrawingOptions())
        CGContextRestoreGState(context)
    }

    
}