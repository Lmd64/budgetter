//
//  Budget+CoreDataProperties.swift
//  Budgetter
//
//  Created by Liam Dunne on 19/10/2015.
//  Copyright © 2015 Lmd64. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Budget {

    @NSManaged public var dateCreated: NSDate?
    @NSManaged public var name: String?
    @NSManaged public var users: NSSet?

}
