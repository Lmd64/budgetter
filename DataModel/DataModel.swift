//
//  DataModel.swift
//  Budgetter
//
//  Created by Liam Dunne on 19/10/2015.
//  Copyright © 2015 Lmd64. All rights reserved.
//

import UIKit
import CoreData

public class DataModel: NSObject {

    public static let sharedInstance = DataModel()

    public lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "Lmd64.ImageSeq" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1]
        //        return urls[urls.count-1] as NSURL
        }()
    
    public lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        //let modelURL = NSBundle.mainBundle().URLForResource("Budgetter", withExtension: "momd")

        let bundleURL = NSBundle(forClass: DataModel.self).bundleURL
        let modelURL = bundleURL.URLByAppendingPathComponent("Budgetter.momd")
        
        let model = NSManagedObjectModel(contentsOfURL: modelURL)
        return model!
        
        }()
    
    public lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
        // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("Budgetter.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        
        let options = [NSMigratePersistentStoresAutomaticallyOption:true, NSInferMappingModelAutomaticallyOption:true]

        
        do {
            try coordinator!.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: options)
        } catch {
            coordinator = nil
            // Report any error we got.
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            print(error)
            abort()
        }
        
        return coordinator
        }()
    
    public lazy var managedObjectContext: NSManagedObjectContext? = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        if coordinator == nil {
            return nil
        }
        var managedObjectContext = NSManagedObjectContext.init(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
        }()

    public var fetchedResultsController: NSFetchedResultsController {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let fetchRequest = NSFetchRequest()
        // Edit the entity name as appropriate.
        let entity = NSEntityDescription.entityForName("Budget", inManagedObjectContext: self.managedObjectContext!)
        fetchRequest.entity = entity
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Edit the sort key as appropriate.
        let sortDescriptor = NSSortDescriptor(key: "dateCreated", ascending: false)
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        NSFetchedResultsController.deleteCacheWithName("Master")
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObjectContext!, sectionNameKeyPath: nil, cacheName: "Master")
        _fetchedResultsController = aFetchedResultsController
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            print(error)
        }
        
        return _fetchedResultsController!
    }
    var _fetchedResultsController: NSFetchedResultsController? = nil

    var _usersFetchedResultsController: NSFetchedResultsController? = nil
    public func usersFetchedResultsController(budget :Budget) -> NSFetchedResultsController {
        if _usersFetchedResultsController != nil {
            return _usersFetchedResultsController!
        }
        
        let fetchRequest = NSFetchRequest()
        // Edit the entity name as appropriate.
        let entity = NSEntityDescription.entityForName("User", inManagedObjectContext: self.managedObjectContext!)
        fetchRequest.entity = entity
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Edit the sort key as appropriate.
        let sortDescriptor = NSSortDescriptor(key: "dateCreated", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let predicate = NSPredicate(format: "any budget = %@", budget)
        fetchRequest.predicate = predicate
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        NSFetchedResultsController.deleteCacheWithName("Master")
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObjectContext!, sectionNameKeyPath:nil, cacheName: "Master")
        _usersFetchedResultsController = aFetchedResultsController
        
        do {
            try _usersFetchedResultsController!.performFetch()
        } catch {
            print(error)
        }
        
        return _usersFetchedResultsController!
    }
    

    var _costItemsFetchedResultsController: NSFetchedResultsController? = nil
    public func costItemsFetchedResultsController(budget :Budget, user :User) -> NSFetchedResultsController {
//        if _costItemsFetchedResultsController != nil {
//            return _costItemsFetchedResultsController!
//        }
        
        let fetchRequest = NSFetchRequest()
        // Edit the entity name as appropriate.
        let entity = NSEntityDescription.entityForName("CostItem", inManagedObjectContext: self.managedObjectContext!)
        fetchRequest.entity = entity
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Edit the sort key as appropriate.
        let sortDescriptor1 = NSSortDescriptor(key: "index", ascending: true)
        let sortDescriptor2 = NSSortDescriptor(key: "dateAdded", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor1,sortDescriptor2]
        
        let predicate = NSPredicate(format: "any user.budget = %@ AND user = %@", budget, user)
        //let predicate = NSPredicate(format: "user.budget = %@ AND user = %@", budget, user)
        fetchRequest.predicate = predicate
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        NSFetchedResultsController.deleteCacheWithName("Master")
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObjectContext!, sectionNameKeyPath:"user", cacheName: "Master")
        _costItemsFetchedResultsController = aFetchedResultsController
        
        do {
            try _costItemsFetchedResultsController!.performFetch()
        } catch {
            print(error)
        }
        
        return _costItemsFetchedResultsController!
    }
    
    public func saveContext(){

        if managedObjectContext!.hasChanges {
            do {
                try managedObjectContext!.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }

    }
    
    public class func scrubAllContent(){
        Budget.deleteAll()
        User.deleteAll()
        CostItem.deleteAll()
    }

    public class func logSummary() -> String {
        var logSummary = ""
        logSummary = logSummary.stringByAppendingString("DataModel count:\n")
        logSummary = logSummary.stringByAppendingString("Budget count   = \(Budget.countOfAllObjects())\n")
        logSummary = logSummary.stringByAppendingString("User count     = \(User.countOfAllObjects())\n")
        logSummary = logSummary.stringByAppendingString("CostItem count = \(CostItem.countOfAllObjects())\n")
        return logSummary
    }
    
    public class func logDescription() -> String {
        var logDescription = ""
        
        for item in Budget.allObjects() {
            logDescription = logDescription.stringByAppendingString("\(item.logDescription())\n")
        }
        for item in User.allObjects() {
            logDescription = logDescription.stringByAppendingString("\(item.logDescription())\n")
        }
        for item in CostItem.allObjects() {
            logDescription = logDescription.stringByAppendingString("\(item.logDescription())\n")
        }
        
        return logDescription
    }

    public class func prepareTestData() {
        
        scrubAllContent()
        
        let budget1 = Budget.new("Budget 1")
        sharedInstance.saveContext()
        let user1 = User.new("User 1")
        budget1.addUser(user1)
        sharedInstance.saveContext()
        let user2 = User.new("User 2")
        budget1.addUser(user2)
        sharedInstance.saveContext()
        for i in 0...9 {
            CostItem.new("Item \(i)", amount: Double(i)+0.99, user: user1)
        }
        for i in 0...5 {
            CostItem.new("Item \(i+10)", amount: (Double(i)+0.99)*10, user: user2)
        }
        sharedInstance.saveContext()

        let budget2 = Budget.new("Budget 2")
        budget2.addUser(user1)
        let user3 = User.new("User 3")
        budget2.addUser(user3)
        let user4 = User.new("User 4")
        budget2.addUser(user4)
        for i in 0...4 {
            CostItem.new("Book \(i)", amount: Double(i)+0.99, user: user1)
        }
        for i in 0...7 {
            CostItem.new("Book \(i+10)", amount: (Double(i)+0.99)*10, user: user3)
        }
        for i in 0...3 {
            CostItem.new("Book \(i*2+50)", amount: (Double(i)+0.99)*10, user: user4)
        }
        
        sharedInstance.saveContext()
        
        print("\(DataModel.logDescription())")
        
        print("\(DataModel.logSummary())")

    }
    
}
