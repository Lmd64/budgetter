//
//  User.swift
//  Budgetter
//
//  Created by Liam Dunne on 19/10/2015.
//  Copyright © 2015 Lmd64. All rights reserved.
//

import Foundation
import CoreData

public class User: NSManagedObject {

    public class func new(name :String) -> User{
        
        let entityName = "User"
        let context = DataModel.sharedInstance.fetchedResultsController.managedObjectContext
        
        let item = NSEntityDescription.insertNewObjectForEntityForName(entityName, inManagedObjectContext: context) as! User
        item.name = name
        item.dateCreated = NSDate()

        do {
            try context.save()
        } catch {
            print(error)
        }
        
        return item
    }
    
    public func logDescription() -> String {
        var logDescription :String = ""
//        if avatar != nil { logDescription = logDescription.stringByAppendingString("avatar: \(avatar)\n")}
//        if dateCreated != nil { logDescription = logDescription.stringByAppendingString("dateCreated: \(dateCreated)\n")}
//        if email != nil { logDescription = logDescription.stringByAppendingString("email: \(email)\n")}
//        if name != nil { logDescription = logDescription.stringByAppendingString("name: \(name)\n")}
//        if budget != nil {logDescription = logDescription.stringByAppendingString("budget: \(budget!.count)\n")}
//        if costItem != nil { logDescription = logDescription.stringByAppendingString("costItem: \(costItem)\n")}
        return logDescription
    }
    
    public class func countOfAllObjects() -> Int {
        
        let entityName = "User"
        let context = DataModel.sharedInstance.fetchedResultsController.managedObjectContext
        
        let fetchRequest = NSFetchRequest(entityName: entityName)
        fetchRequest.sortDescriptors = []
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        do {
            try fetchedResultsController.performFetch()
        } catch {
            print(error)
        }
        let countOfFetchedObjects = (fetchedResultsController.fetchedObjects?.count)! as Int
        return countOfFetchedObjects
    }

    public class func allObjects() -> Array <User> {
        
        let entityName = "User"
        let context = DataModel.sharedInstance.fetchedResultsController.managedObjectContext
        
        let fetchRequest = NSFetchRequest(entityName: entityName)
        fetchRequest.sortDescriptors = []
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        do {
            try fetchedResultsController.performFetch()
        } catch {
            print(error)
        }
        let fetchedObjects = fetchedResultsController.fetchedObjects as! Array<User>
        return fetchedObjects
    }
    
    public class func deleteAll(){
        
        let context = DataModel.sharedInstance.managedObjectContext
        let allObjects = User.allObjects()
        for object in allObjects as [User] {
            context!.deleteObject(object)
        }
        
        do {
            try context!.save()
        } catch {
            print("Unresolved error \(error)")
        }
        
    }

}
