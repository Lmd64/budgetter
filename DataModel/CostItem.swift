//
//  CostItem.swift
//  Budgetter
//
//  Created by Liam Dunne on 19/10/2015.
//  Copyright © 2015 Lmd64. All rights reserved.
//

import Foundation
import CoreData

public class CostItem: NSManagedObject {

    public class func rebuildIndices() {
        var i = 0
        for costItem in CostItem.allObjects() {
            costItem.index = i
            i++
        }
    }
    
    public func logDescription() -> String {
        var logDescription :String = ""
//        if amount != nil {logDescription = logDescription.stringByAppendingString("amount: \(amount!)\n")}
//        if dateAdded != nil {logDescription = logDescription.stringByAppendingString("dateAdded: \(dateAdded!)\n")}
//        if name != nil {logDescription = logDescription.stringByAppendingString("name: \(name!)\n")}
//        if index != nil {logDescription = logDescription.stringByAppendingString("index: \(index!)\n")}
//        if user != nil {logDescription = logDescription.stringByAppendingString("user: \(user!.name!)\n")}
        return logDescription
    }

    public class func new(name :String, amount :Double, user :User) -> CostItem{
        
        let entityName = "CostItem"
        let context = DataModel.sharedInstance.fetchedResultsController.managedObjectContext
        //let entity = NSEntityDescription.entityForName(entityName, inManagedObjectContext: context)
        
        let item = NSEntityDescription.insertNewObjectForEntityForName(entityName, inManagedObjectContext: context) as! CostItem
        item.name = name
        item.amount = amount
        item.dateAdded = NSDate()
        item.user = user
        item.index = CostItem.countOfAllObjects() + 1
        
        do {
            try context.save()
        } catch {
            print(error)
        }
        
        return item
    }

    public class func countOfAllObjects() -> Int {
        
        let entityName = "CostItem"
        let context = DataModel.sharedInstance.fetchedResultsController.managedObjectContext
        
        let fetchRequest = NSFetchRequest(entityName: entityName)
        fetchRequest.sortDescriptors = []
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        do {
            try fetchedResultsController.performFetch()
        } catch {
            print(error)
        }
        let countOfFetchedObjects = (fetchedResultsController.fetchedObjects?.count)! as Int
        return countOfFetchedObjects
    }
    
    public class func allObjects() -> Array <CostItem> {
        
        let entityName = "CostItem"
        let context = DataModel.sharedInstance.fetchedResultsController.managedObjectContext
        
        let fetchRequest = NSFetchRequest(entityName: entityName)
        fetchRequest.sortDescriptors = []
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        do {
            try fetchedResultsController.performFetch()
        } catch {
            print(error)
        }
        let fetchedObjects = fetchedResultsController.fetchedObjects as! Array<CostItem>
        return fetchedObjects
    }
    
    public class func deleteAll(){
        
        let context = DataModel.sharedInstance.managedObjectContext
        let allObjects = CostItem.allObjects()
        for object in allObjects as [CostItem] {
            context!.deleteObject(object)
        }
        
        do {
            try context!.save()
        } catch {
            print("Unresolved error \(error)")
        }
        
    }

    public func amountFormattedString() -> String {
        let value = amount!
        
        let formatter = NSNumberFormatter()
        formatter.numberStyle = .CurrencyAccountingStyle
        formatter.locale = NSLocale.currentLocale()
        
        return formatter.stringFromNumber(value)!
    }

}
