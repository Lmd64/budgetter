//
//  User+CoreDataProperties.swift
//  Budgetter
//
//  Created by Liam Dunne on 22/10/2015.
//  Copyright © 2015 Lmd64. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension User {

    @NSManaged public var avatar: NSData?
    @NSManaged public var dateCreated: NSDate?
    @NSManaged public var email: String?
    @NSManaged public var name: String?
    @NSManaged public var budget: NSSet?
    @NSManaged public var costItem: NSSet?

}
