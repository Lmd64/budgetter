//
//  Budget.swift
//  Budgetter
//
//  Created by Liam Dunne on 19/10/2015.
//  Copyright © 2015 Lmd64. All rights reserved.
//

import Foundation
import CoreData

public class Budget: NSManagedObject {
    
    public class func new(name :String) -> Budget{
        
        let entityName = "Budget"
        let context = DataModel.sharedInstance.fetchedResultsController.managedObjectContext
        //let entity = NSEntityDescription.entityForName(entityName, inManagedObjectContext: context)
        
        let item = NSEntityDescription.insertNewObjectForEntityForName(entityName, inManagedObjectContext: context) as! Budget
        item.name = name
        item.dateCreated = NSDate()
        item.users = NSSet()
        
        do {
            try context.save()
        } catch {
            print("Unresolved error \(error)")
        }

        return item
    }

    public func logDescription() -> String {
        var logDescription :String = ""
        if dateCreated != nil { logDescription = logDescription.stringByAppendingString("dateCreated: \(dateCreated!)\n")}
        if name != nil {logDescription = logDescription.stringByAppendingString("name: \(name!)\n")}
        if users != nil {  logDescription = logDescription.stringByAppendingString("users: \(users!.count)\n")}
        return logDescription
    }

    public class func countOfAllObjects() -> Int {
        
        let entityName = "Budget"
        let context = DataModel.sharedInstance.fetchedResultsController.managedObjectContext
        
        let fetchRequest = NSFetchRequest(entityName: entityName)
        fetchRequest.sortDescriptors = []
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        do {
            try fetchedResultsController.performFetch()
        } catch {
            print(error)
        }
        let countOfFetchedObjects = (fetchedResultsController.fetchedObjects?.count)! as Int
        return countOfFetchedObjects
    }

    public class func allObjects() -> Array <Budget> {

        let entityName = "Budget"
        let context = DataModel.sharedInstance.fetchedResultsController.managedObjectContext

        let fetchRequest = NSFetchRequest(entityName: entityName)
        fetchRequest.sortDescriptors = []
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        do {
            try fetchedResultsController.performFetch()
        } catch {
            print(error)
        }
        let fetchedObjects = fetchedResultsController.fetchedObjects as! Array<Budget>
        return fetchedObjects
    }
    
    public class func deleteAll(){
        
        let context = DataModel.sharedInstance.managedObjectContext
        let allObjects = Budget.allObjects()
        for object in allObjects as [Budget] {
            context!.deleteObject(object)
        }
    
        do {
            try context!.save()
        } catch {
            print("Unresolved error \(error)")
        }
    
    }
    
    public func addUser(user:User) {
        let users = self.users?.mutableCopy() as! NSMutableSet
        users.addObject(user)
        self.users = users.copy() as? NSSet
    }
    
    public func removeUser(user:User) {
        let users = self.users?.mutableCopy() as! NSMutableSet
        users.removeObject(user)
        self.users = users.copy() as? NSSet
    }
    
    public func total() -> Double {
        var total = 0.0
        for user in self.users!.allObjects as! [User] {
            for costItem in user.costItem!.allObjects as! [CostItem]  {
                total += (costItem.amount?.doubleValue)!
            }
        }
        return total
    }

    public func totalForUser(user :User) -> Double {
        var total = 0.0
        for costItem in user.costItem!.allObjects as! [CostItem]  {
            total += (costItem.amount?.doubleValue)!
        }
        return total
    }

    public func percentForUser(user :User) -> Double {
        let userTotal = totalForUser(user)
        let budgetTotal = total()
        let percent = userTotal / budgetTotal
        return percent
    }
    
    public func totalFormattedString() -> String {
        let value = total() 

        let formatter = NSNumberFormatter()
        formatter.numberStyle = .CurrencyAccountingStyle
        formatter.locale = NSLocale.currentLocale()
        
        return formatter.stringFromNumber(value)!
    }
    
    public func totalFormattedStringForUser(user :User) -> String {
        let value = totalForUser(user)
        
        let formatter = NSNumberFormatter()
        formatter.numberStyle = .CurrencyAccountingStyle
        formatter.locale = NSLocale.currentLocale()
        
        return formatter.stringFromNumber(value)!
    }
    
    public func percentFormattedStringForUser(user :User) -> String {
        let value = percentForUser(user)

        let formatter = NSNumberFormatter()
        formatter.numberStyle = .PercentStyle
        formatter.locale = NSLocale.currentLocale()
        
        return formatter.stringFromNumber(value)!
    }

    public func allCostItems() -> [CostItem] {
        var costItems :[CostItem] = []
        for user in users!.allObjects as! [User]  {
            for costItem in user.costItem!.allObjects as! [CostItem]  {
                costItems.append(costItem)
            }
        }
        let sortedCostItems = costItems.sort() {
            (obj1, obj2) in
            let item1 = obj1 as CostItem
            let item2 = obj2 as CostItem
            return item1.index?.integerValue < item2.index?.integerValue
        }
        return sortedCostItems
    }
    

    public func csvOutput() -> (String, String) {

        var csv = ""
        
        //filename
        if name == nil {
            return ("","")
        } else {
        let filename = "\(name)-\(dateCreated).csv"
        
        
        if let allUsers = users?.allObjects as? [User] {

            //headings
            csv.write("Cost Item,")
            for user in allUsers {
                csv.write("\(user.name!),")
            }
            csv.write("\n")
            
            //rows
            let allCostItems = self.allCostItems()
            for costItem in allCostItems as [CostItem] {
                csv.write("\(costItem.name!),")
                for user in allUsers {
                    if costItem.user == user {
                        csv.write("\(costItem.amount!),")
                    } else {
                        csv.write(",")
                    }
                }
                csv.write("\n")
            }
        
        }
        
    
        return (filename, csv)
        }
    }
    
}
