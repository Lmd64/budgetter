//
//  BudgetterTests.swift
//  BudgetterTests
//
//  Created by Liam Dunne on 19/10/2015.
//  Copyright © 2015 Lmd64. All rights reserved.
//

import XCTest
@testable import Budgetter
@testable import DataModel

class BudgetterTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        DataModel.scrubAllContent()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSimpleBudget() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.

        print("count of budgets \(Budget.allObjects().count)")
        
        let budget = Budget.new("New Budget")

        let user1 = User.new("User1")
        budget.addUser(user1)
        
        let user2 = User.new("User2")
        budget.addUser(user2)

        let item1Amount = 23.99
        let item2Amount = 89.99
        let item3Amount = 159.99
        let totalAmount = item1Amount + item2Amount + item3Amount
        
        CostItem.new("hat", amount: item1Amount, user: user1)
        CostItem.new("bag", amount: item2Amount, user: user2)
        CostItem.new("shoes", amount: item3Amount, user: user1)

        print("budget    = \(budget.name)")
        print("user1     = \(user1.name)")
        print("user2     = \(user2.name)")

        print("total = \(budget.totalFormattedString())")
        print("totalForUser1 = \(budget.totalFormattedStringForUser(user1))")
        print("totalForUser2 = \(budget.totalFormattedStringForUser(user2))")
        print("percentForUser1 = \(budget.percentFormattedStringForUser(user1))")
        print("percentForUser2 = \(budget.percentFormattedStringForUser(user2))")

        let total = budget.total()
        XCTAssertEqual(total, totalAmount)
        
    }
    
    func testSimpleBudgetPerformance() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
            self.testSimpleBudget()
        }
    }
    
}
